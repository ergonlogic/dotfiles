# some more ls aliases
alias l='ls -lhaF --color=always --show-control-chars'
#alias ll='ls -lhaF --color=always --show-control-chars |less -R'
alias lt='ls -lhatF --color=always --show-control-chars'
alias llt='ls -lhatF --color=always --show-control-chars |less -R'
#alias ll='ls -alF'
alias la='ls -A'
#alias l='ls -CF'

#alias .='pwd'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'
alias ......='cd ../../../../..'

# list just hidden files or directories
alias lh='ls -alF | egrep "^[-].*.[ ]\..*$" --color=none'
alias ld='ls -alF | egrep "^[d].*.[ ]\..*$" --color=none'

# Git
alias diffc='git diff --cached'

# Drush
alias drush4='cd ~/code/drush/drush && git checkout --quiet 7.x-4.6 && cd - >/dev/null'
alias drush5='cd ~/code/drush/drush && git checkout --quiet 7.x-5.7 && cd - >/dev/null'
alias drush6='cd ~/code/drush/drush && git checkout --quiet master && cd - >/dev/null'
alias dr='drush'

# Vagrant
alias vt='vagrant'

# Suspend
alias off='sudo pm-suspend'

# Turn integrated webcam on/off
alias camera_off='sudo modprobe -r uvcvideo'
alias camera_on='sudo modprobe uvcvideo'

# Fix flaky touchpad
alias fix_mouse='sudo modprobe -r psmouse && sudo modprobe psmouse'
alias fix_mouse2='gsettings set org.gnome.settings-daemon.plugins.cursor active false'

alias fix_printer='sudo service cups restart'

alias fix_scanner='sudo hp-plugin'

alias fix_sound='pulseaudio -k ; pulseaudio -D'

# tether to android phone
alias tether="sudo ~/scripts/tether.sh"
alias go-camden='ssh aegir.camden-dev.com'
