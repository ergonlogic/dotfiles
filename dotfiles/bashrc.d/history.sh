## ref.: http://xdeb.org/node/1390

# Settings for history function
export HISTFILESIZE=5000
export HISTSIZE=5000
export HISTCONTROL=ignoreboth:erasedups
export HISTIGNORE='\&:e:c:l:ca:cd:cd -'

# Make history work well with multiple shells
# append to the history file, don't overwrite it
shopt -s histappend
