# Include Drush bash customizations.
if [ -f "/home/ergonlogic/.drush/drush.bashrc" ] ; then
  source /home/ergonlogic/.drush/drush.bashrc
fi

# Include Drush completion.
if [ -f "/home/ergonlogic/.drush/drush.complete.sh" ] ; then
  source /home/ergonlogic/.drush/drush.complete.sh
fi

# Include Drush prompt customizations.
if [ -f "/home/ergonlogic/.drush/drush.prompt.sh" ] ; then
#  source /home/ergonlogic/.drush/drush.prompt.sh
fi

