# i3 config file (v4)
#
# Please see http://i3wm.org/docs/userguide.html for a complete reference!

# Set up monitors
exec xrandr --output HDMI-2 --auto --left-of DVI-0

set $mod Mod4

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
font pango:Droid Sans 13

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# kill focused window
bindsym $mod+Shift+q kill

# change focus
bindsym $mod+j focus left
bindsym $mod+k focus down
bindsym $mod+l focus up
bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+j move left
bindsym $mod+Shift+k move down
bindsym $mod+Shift+l move up
bindsym $mod+Shift+semicolon move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+h split h

# split in vertical orientation
bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

for_window [class="Pavucontrol"] floating enable

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child


###################################
#                                 #
#           WORKSPACES            #     
#                                 #
###################################


## Coding workspaces (on the left)

## Terminals ##
set $workspace1 "1 >_"
assign [class="Xfce4-terminal" title="Terminal"] → $workspace1
exec xfce4-terminal --default-working-directory=/home/ergonlogic/code

## Git repos ##
set $workspace3 "3 "
#assign [class="google-chrome" id="GitHub"] → $workspace3
assign [id="GitHub"] → $workspace3
#assign [class="google-chrome" id="GitLab"] → $workspace3
assign [id="GitLab"] → $workspace3
#exec_always google-chrome https://github.com/ergonlogic
exec google-chrome https://github.com/ergonlogic
exec google-chrome https://github.com/aegir-project
exec google-chrome https://gitlab.com/Aegir

## Firefox (main browser) ##
set $workspace5 "5 "
assign [class="Firefox" id="Start Page"] → $workspace5

## Documentation ##
set $workspace7 "7 "


## Communication workspaces (on the right)

## Email ##
set $workspace2 "2 "
assign [class="Icedove"] → $workspace2
assign [class="Pinentry-gtk-2"] → $workspace2
exec icedove

## IRC, Hangouts, Slack ##
set $workspace4 "4 "
assign [class="Xfce4-terminal" title=" irssi"] → $workspace4
exec xfce4-terminal --title=" irssi" -x irssi
exec ~/.local/bin/notify-listener.py & # Run a listener for IRC mentions.
assign [instance="openproducer.slack.com"] → $workspace4
exec google-chrome --app=https://openproducer.slack.com
assign [instance="nditech.slack.com"] → $workspace4
exec google-chrome --app=https://nditech.slack.com
assign [instance="hangouts.google.com"] → $workspace4
exec google-chrome --app=https://hangouts.google.com

## Social media & Chrome (secondary browser) ##
set $workspace6 "6 "
assign [class="choqok"] → $workspace6
exec --no-startup-id i3-msg "workspace $workspace6 ; exec choqok"

## Miscellaneous workspaces

set $workspace8 "8"
set $workspace9 "9"
set $workspace0 "0"


# Workspaces
# Odd numbers on the left (HDMI-2), even on the right (DVI-0)
workspace $workspace1 output HDMI-2
workspace $workspace2 output DVI-0
workspace $workspace3 output HDMI-2
workspace $workspace4 output DVI-0
workspace $workspace5 output HDMI-2
workspace $workspace6 output DVI-0
workspace $workspace7 output HDMI-2
workspace $workspace8 output DVI-0
workspace $workspace9 output HDMI-2
workspace $workspace0 output DVI-0

# switch to workspace
bindsym $mod+0 workspace $workspace0
bindsym $mod+1 workspace $workspace1
bindsym $mod+2 workspace $workspace2
bindsym $mod+3 workspace $workspace3
bindsym $mod+4 workspace $workspace4
bindsym $mod+5 workspace $workspace5
bindsym $mod+6 workspace $workspace6
bindsym $mod+7 workspace $workspace7
bindsym $mod+8 workspace $workspace8
bindsym $mod+9 workspace $workspace9

# move focused container to workspace
bindsym $mod+Shift+0 move container to workspace $workspace0
bindsym $mod+Shift+1 move container to workspace $workspace1
bindsym $mod+Shift+2 move container to workspace $workspace2
bindsym $mod+Shift+3 move container to workspace $workspace3
bindsym $mod+Shift+4 move container to workspace $workspace4
bindsym $mod+Shift+5 move container to workspace $workspace5
bindsym $mod+Shift+6 move container to workspace $workspace6
bindsym $mod+Shift+7 move container to workspace $workspace7
bindsym $mod+Shift+8 move container to workspace $workspace8
bindsym $mod+Shift+9 move container to workspace $workspace9

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
#bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"
bindsym $mod+Shift+e exec ~/.i3/scripts/shutdown_menu

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

set $bg-color 	         #2f343f
set $inactive-bg-color   #2f343f
set $text-color          #f3f4f5
set $inactive-text-color #676E7D
set $urgent-bg-color     #E53935
set $rofi-fg             #9575cd

# window colors
#                       border              background         text                 indicator
client.focused          $bg-color           $bg-color          $text-color          #00ff00
client.unfocused        $inactive-bg-color  $inactive-bg-color $inactive-text-color #00ff00
client.focused_inactive $inactive-bg-color  $inactive-bg-color $inactive-text-color #00ff00
client.urgent           $urgent-bg-color    $urgent-bg-color   $text-color          #00ff00

# See: ~/.Xresources for Rofi config
bindsym $mod+d exec rofi -show run
bindsym $mod+x exec rofi -show ssh

# Add nice fade transitions & transparency
exec --no-startup-id compton -f

# Re-map caps-lock to an extra control key
exec --no-startup-id setxkbmap -layout us -option ctrl:nocaps

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
    status_command i3blocks -c ~/.i3/i3blocks.conf
}

# Set wallpaper
exec_always feh --bg-scale /home/ergonlogic/Downloads/sunset_horizon.jpg



# Raise volume.
#bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume 2 +5%
# Lower volume. 
#bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume 2 -5%
# Mute sound.
#bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 2 toggle

